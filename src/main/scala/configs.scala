package diffeq

import Chisel._
import freechips.rocketchip.config._
import freechips.rocketchip.diplomacy._
import freechips.rocketchip.rocket._
import freechips.rocketchip.tile._
import freechips.rocketchip.tilelink._

/*
 * Spfp/Dpfp - single/double precision floating point
 * IntRound - round output to 16bit integer
 * Sc/Dc/Qc - Single/Dual/Quad compute units
 * Ss/Ds/Qs/10s - Single/Dual/Quad/10 stage compute
 * P - piplined
 */
trait ConfigType
case object EulerSc extends ConfigType
case object EulerScP extends ConfigType
case object EulerDc extends ConfigType
case object EulerDpfpSc extends ConfigType
case object EulerIntRoundSc extends ConfigType
case object EulerIntRoundDc extends ConfigType
case object EulerIntRoundQc extends ConfigType
case object RK2Ss extends ConfigType
case object RK2Ds extends ConfigType
case object RK2DsP extends ConfigType
case object RK2Qs extends ConfigType
case object RK2DpfpQs extends ConfigType
case object RK2QsP extends ConfigType
case object RK25sP extends ConfigType
case object RK27sP extends ConfigType
case object RK210sP extends ConfigType
case object RK220sP extends ConfigType
case object RK4Ss extends ConfigType
case object RK4Ds extends ConfigType
case object RK4Qs extends ConfigType

case object DiffEqConfig extends Field[ConfigType]

class WithDiffEqAccel(config: ConfigType = EulerSc) extends Config((site, here, up) => {
  case DiffEqConfig => config
  case BuildRoCC => up(BuildRoCC) ++ Seq(
    (p: Parameters) => {
      val diffeq = LazyModule.apply(new DiffEqAccel(OpcodeSet.custom2)(p))
      diffeq
    }
  )
})

class DiffEqAccel(opcodes: OpcodeSet)(implicit p: Parameters) extends LazyRoCC(opcodes) {
  val config = p(DiffEqConfig)
  override lazy val module = config match {
    case EulerSc          => new AccelConfEulerSc(this)
    case EulerScP         => new AccelConfEulerScP(this)
    case EulerDc          => new AccelConfEulerDc(this)
    case EulerDpfpSc      => new AccelConfEulerDpfpSc(this)
    case EulerIntRoundSc  => new AccelConfEulerIntRoundSc(this)
    case EulerIntRoundDc  => new AccelConfEulerIntRoundDc(this)
    case EulerIntRoundQc  => new AccelConfEulerIntRoundQc(this)
    case RK2Ss            => new AccelConfRKSs(this)
    case RK2Ds            => new AccelConfRKDs(this)
    case RK2DsP           => new AccelConfRKDsP(this)
    case RK2Qs            => new AccelConfRKQs(this)
    case RK2DpfpQs        => new AccelConfRKDpfpQs(this)
    case RK2QsP           => new AccelConfRKQsP(this)
    case RK25sP           => new AccelConfRK25sP(this)
    case RK27sP           => new AccelConfRK27sP(this)
    case RK210sP          => new AccelConfRK210sP(this)
    case RK220sP          => new AccelConfRK220sP(this)
    case RK4Ss            => new AccelConfRKSs(this, use_rk4=true)
    case RK4Ds            => new AccelConfRKDs(this, use_rk4=true)
    case RK4Qs            => new AccelConfRKQs(this, use_rk4=true)
  }
}

class CommonSpfp(outer: DiffEqAccel) extends DiffEqAccelImp(outer) {
  val s_write = 4.U
  io.mem.req.valid := mem_req_valid_common || r_state === s_write
  common_states(s_next=s_write)

  def sf_write_spfp(s_next: UInt) = {
    sf_write(data=Cat(r_y, r_x), s_next=s_next, end_num=Some(r_num_steps))
  }
}

class AccelConfEulerSc(outer: DiffEqAccel) extends CommonSpfp(outer) {
  val s_compute_x = 5.U
  val s_compute_y = 6.U

  val r_x_prev = Reg(Bits(float_width))
  val euler_unit = Module(new LVEuler(fpu))

  switch(r_state) {
    is(s_write) { sf_write_spfp(s_next=s_compute_x) }
    is(s_compute_x) {
      r_x_prev := euler_x(euler_unit, r_x, r_y)
      r_state := s_compute_y
    }
    is(s_compute_y) {
      r_y := euler_y(euler_unit, r_x, r_y)
      r_x := r_x_prev
      r_state := s_write
    }
  }
}

class AccelConfEulerScP(outer: DiffEqAccel) extends CommonSpfp(outer) {
  val s_write_compute_x = 4.U
  val s_compute_y = 5.U

  val r_x_prev = Reg(Bits(float_width))
  val euler_unit = Module(new LVEuler(fpu))

  switch(r_state) {
    is(s_write_compute_x) {
      sf_write_spfp(s_next=s_compute_y)
      r_x_prev := euler_x(euler_unit, r_x, r_y)
    }
    is(s_compute_y) {
      r_y := euler_y(euler_unit, r_x, r_y)
      r_x := r_x_prev
      r_state := s_write_compute_x
    }
  }
}

class AccelConfEulerDc(outer: DiffEqAccel) extends CommonSpfp(outer) {
  val s_compute = 5.U

  val euler_unit_x = Module(new LVEuler(fpu))
  val euler_unit_y = Module(new LVEuler(fpu))

  switch(r_state) {
    is(s_write) { sf_write_spfp(s_next=s_compute) }
    is(s_compute) {
      val (x,y) = euler_x_y(euler_unit_x, euler_unit_y, r_x, r_y)
      r_x := x
      r_y := y
      r_state := s_write
    }
  }
}

class AccelConfEulerDpfpSc(outer: DiffEqAccel) extends DiffEqAccelImp(outer, use_dpfp=true) {
  val s_write_x = 4.U
  val s_compute_x = 5.U
  val s_write_y = 6.U
  val s_compute_y = 7.U

  val r_x_prev = Reg(Bits(float_width))
  val euler_unit = Module(new LVEuler(fpu))

  io.mem.req.valid := mem_req_valid_common || r_state === s_write_x || r_state === s_write_y

  common_states(s_next=s_write_x, dpfp=true)

  switch(r_state) {
    is(s_write_x) { sf_write(data=r_x, s_next=s_compute_x) }
    is(s_compute_x) {
      r_x_prev := euler_x(euler_unit, r_x, r_y)
      r_state := s_write_y
    }
    is(s_write_y) { sf_write(data=r_y, s_next=s_compute_y, end_num=Some(r_num_steps * 2.U)) }
    is(s_compute_y) {
      r_y := euler_y(euler_unit, r_x, r_y)
      r_x := r_x_prev
      r_state := s_write_x
    }
  }
}

class CommonIntRound(outer: DiffEqAccel) extends CommonSpfp(outer) {
  val r_x_prev = Reg(Bits(0, float_width))
  val r_y_prev = Reg(Bits(0, float_width))

  val r_x_int = fpu.recfp_to_int(fpu.fp_to_recfp(r_x), 16)
  val r_y_int = fpu.recfp_to_int(fpu.fp_to_recfp(r_y), 16)
  val r_x_prev_int = fpu.recfp_to_int(fpu.fp_to_recfp(r_x_prev), 16)
  val r_y_prev_int = fpu.recfp_to_int(fpu.fp_to_recfp(r_y_prev), 16)

  def sf_write_int_round(s_next: UInt) = {
    sf_write(
      data=Cat(r_y_int, r_x_int, r_y_prev_int, r_x_prev_int),
      s_next=s_next,
      end_num=Some((r_num_steps + 1.U) / 2.U)
    )
  }
}

class AccelConfEulerIntRoundSc(outer: DiffEqAccel) extends CommonIntRound(outer) {
  val s_compute_x0 = 5.U
  val s_compute_y0 = 6.U
  val s_compute_x1 = 7.U
  val s_compute_y1 = 8.U

  val euler_unit = Module(new LVEuler(fpu))

  switch(r_state) {
    is(s_write) { sf_write_int_round(s_compute_x0) }
    is(s_compute_x0) {
      r_x_prev := euler_x(euler_unit, r_x, r_y)
      r_state := s_compute_y0
    }
    is(s_compute_y0) {
      r_y_prev := euler_y(euler_unit, r_x, r_y)
      r_state := s_compute_x1
    }
    is(s_compute_x1) {
      r_x := euler_x(euler_unit, r_x_prev, r_y_prev)
      r_state := s_compute_y1
    }
    is(s_compute_y1) {
      r_y := euler_y(euler_unit, r_x_prev, r_y_prev)
      r_state := s_write
    }
  }
}

class AccelConfEulerIntRoundDc(outer: DiffEqAccel) extends CommonIntRound(outer) {
  val s_compute0 = 5.U
  val s_compute1 = 6.U

  val euler_unit_x = Module(new LVEuler(fpu))
  val euler_unit_y = Module(new LVEuler(fpu))

  switch(r_state) {
    is(s_write) { sf_write_int_round(s_compute0) }
    is(s_compute0) {
      val (x_prev, y_prev) = euler_x_y(euler_unit_x, euler_unit_y, r_x, r_y)
      r_x_prev := x_prev
      r_y_prev := y_prev
      r_state := s_compute1
    }
    is(s_compute1) {
      val (x, y) = euler_x_y(euler_unit_x, euler_unit_y, r_x_prev, r_y_prev)
      r_x := x
      r_y := y
      r_state := s_write
    }
  }
}

class AccelConfEulerIntRoundQc(outer: DiffEqAccel) extends CommonIntRound(outer) {
  val s_compute = 5.U

  val euler_unit_x0 = Module(new LVEuler(fpu))
  val euler_unit_y0 = Module(new LVEuler(fpu))
  val euler_unit_x1 = Module(new LVEuler(fpu))
  val euler_unit_y1 = Module(new LVEuler(fpu))

  switch(r_state) {
    is(s_write) { sf_write_int_round(s_compute) }
    is(s_compute) {
      val (x_prev, y_prev) = euler_x_y(euler_unit_x0, euler_unit_y0, r_x, r_y)
      r_x_prev := x_prev
      r_y_prev := y_prev
      val (x, y) = euler_x_y(euler_unit_x1, euler_unit_y1, x_prev, y_prev)
      r_x := x
      r_y := y
      r_state := s_write
    }
  }
}

class AccelConfRKSs(outer: DiffEqAccel, use_rk4: Boolean = false) extends CommonSpfp(outer) {
  val s_compute = 5.U

  val rk_unit = if(use_rk4) Module(new LVRK4_1Stage(fpu)) else Module(new LVRK2_1Stage(fpu))
  rk_set_inputs(rk_unit)

  switch(r_state) {
    is(s_write) { sf_write_spfp(s_next=s_compute) }
    is(s_compute) {
      rk_unit.io.stage := 0.U
      r_x := rk_unit.io.x_np1
      r_y := rk_unit.io.y_np1
      r_state := s_write
    }
  }
}

class AccelConfRKDs(outer: DiffEqAccel, use_rk4: Boolean = false) extends CommonSpfp(outer) {
  val s_compute0 = 5.U
  val s_compute1 = 6.U

  val rk_unit = if(use_rk4) Module(new LVRK4_2Stage(fpu)) else Module(new LVRK2_2Stage(fpu))
  rk_set_inputs(rk_unit)

  switch(r_state) {
    is(s_write) { sf_write_spfp(s_next=s_compute0) }
    is(s_compute0) {
      rk_unit.io.stage := 0.U
      r_state := s_compute1
    }
    is(s_compute1) {
      rk_unit.io.stage := 1.U
      r_x := rk_unit.io.x_np1
      r_y := rk_unit.io.y_np1
      r_state := s_write
    }
  }
}

class AccelConfRKDsP(outer: DiffEqAccel, use_rk4: Boolean = false) extends CommonSpfp(outer) {
  val s_write_compute0 = 4.U
  val s_compute1 = 5.U

  val rk_unit = if(use_rk4) Module(new LVRK4_2Stage(fpu)) else Module(new LVRK2_2Stage(fpu))
  rk_set_inputs(rk_unit)

  io.mem.req.valid := mem_req_valid_common || r_state === s_write_compute0

  switch(r_state) {
    is(s_write_compute0) {
      sf_write_spfp(s_next=s_compute1)
      rk_unit.io.stage := 0.U
    }
    is(s_compute1) {
      rk_unit.io.stage := 1.U
      r_x := rk_unit.io.x_np1
      r_y := rk_unit.io.y_np1
      r_state := s_write_compute0
    }
  }
}

class AccelConfRKQs(outer: DiffEqAccel, use_rk4: Boolean = false) extends CommonSpfp(outer) {
  val s_compute0 = 5.U
  val s_compute1 = 6.U
  val s_compute2 = 7.U
  val s_compute3 = 8.U

  val rk_unit = if(use_rk4) Module(new LVRK4_4Stage(fpu)) else Module(new LVRK2_4Stage(fpu))
  rk_set_inputs(rk_unit)

  switch(r_state) {
    is(s_write) { sf_write_spfp(s_next=s_compute0) }
    is(s_compute0) {
      rk_unit.io.stage := 0.U
      r_state := s_compute1
    }
    is(s_compute1) {
      rk_unit.io.stage := 1.U
      r_state := s_compute2
    }
    is(s_compute2) {
      rk_unit.io.stage := 2.U
      r_state := s_compute3
    }
    is(s_compute3) {
      rk_unit.io.stage := 3.U
      r_x := rk_unit.io.x_np1
      r_y := rk_unit.io.y_np1
      r_state := s_write
    }
  }
}

class AccelConfRKQsP(outer: DiffEqAccel, use_rk4: Boolean = false) extends CommonSpfp(outer) {
  val s_compute3 = 5.U

  val sw_compute0 = 0.U
  val sw_compute1 = 1.U
  val sw_compute2 = 2.U
  val sw_wait = 3.U
  val r_wstate = Reg(init=sw_compute0)

  val rk_unit = if(use_rk4) Module(new LVRK4_4Stage(fpu)) else Module(new LVRK2_4Stage(fpu))
  rk_set_inputs(rk_unit)

  switch(r_state) {
    is(s_write) {
      sf_write_spfp(s_next=s_compute3)

      switch(r_wstate) {
        is(sw_compute0) {
          rk_unit.io.stage := 0.U
          r_wstate := sw_compute1
        }
        is(sw_compute1) {
          rk_unit.io.stage := 1.U
          r_wstate := sw_compute2
        }
        is(sw_compute2) {
          rk_unit.io.stage := 2.U
          r_wstate := sw_wait
        }
        is(sw_wait) {
          rk_unit.io.stage := 4.U //inhibit stage
        }
      }
    }
    is(s_compute3) {
      rk_unit.io.stage := 3.U
      r_x := rk_unit.io.x_np1
      r_y := rk_unit.io.y_np1
      r_state := s_write
      r_wstate := sw_compute0
    }
  }
}

class AccelConfRKDpfpQs(outer: DiffEqAccel, use_rk4: Boolean = false) extends DiffEqAccelImp(outer, use_dpfp=true) {
  val s_write_x = 4.U
  val s_compute0 = 5.U
  val s_compute1 = 6.U
  val s_write_y = 7.U
  val s_compute2 = 8.U
  val s_compute3 = 9.U

  val rk_unit = if(use_rk4) Module(new LVRK4_4Stage(fpu)) else Module(new LVRK2_4Stage(fpu))
  rk_set_inputs(rk_unit)

  io.mem.req.valid := mem_req_valid_common || r_state === s_write_x || r_state === s_write_y

  switch(r_state) {
    is(s_write_x) { sf_write(data=r_x, s_next=s_compute0) }
    is(s_compute0) {
      rk_unit.io.stage := 0.U
      r_state := s_compute1
    }
    is(s_compute1) {
      rk_unit.io.stage := 1.U
      r_state := s_write_y
    }
    is(s_write_y) { sf_write(data=r_y, s_next=s_compute2, end_num=Some(r_num_steps * 2.U)) }
    is(s_compute2) {
      rk_unit.io.stage := 2.U
      r_state := s_compute3
    }
    is(s_compute3) {
      rk_unit.io.stage := 3.U
      r_x := rk_unit.io.x_np1
      r_y := rk_unit.io.y_np1
      r_state := s_write_x
    }
  }
}

class AccelConfRK25sP(outer: DiffEqAccel) extends CommonSpfp(outer) {
  val s_compute3 = 5.U
  val s_compute4 = 6.U

  val sw_compute0 = 0.U
  val sw_compute1 = 1.U
  val sw_compute2 = 2.U
  val sw_wait = 3.U
  val r_wstate = Reg(init=sw_compute0)

  val rk_unit = Module(new LVRK2_5Stage(fpu))
  rk_set_inputs(rk_unit)

  switch(r_state) {
    is(s_write) {
      sf_write_spfp(s_next=s_compute3)

      switch(r_wstate) {
        is(sw_compute0) {
          rk_unit.io.stage := 0.U
          r_wstate := sw_compute1
        }
        is(sw_compute1) {
          rk_unit.io.stage := 1.U
          r_wstate := sw_compute2
        }
        is(sw_compute2) {
          rk_unit.io.stage := 2.U
          r_wstate := sw_wait
        }
        is(sw_wait) {
          rk_unit.io.stage := 5.U //inhibit stage
        }
      }
    }
    is(s_compute3) {
      rk_unit.io.stage := 3.U
      r_state := s_compute4
    }
    is(s_compute4) {
      rk_unit.io.stage := 4.U
      r_x := rk_unit.io.x_np1
      r_y := rk_unit.io.y_np1
      r_state := s_write
      r_wstate := sw_compute0
    }
  }
}

class AccelConfRK27sP(outer: DiffEqAccel) extends CommonSpfp(outer) {
  val s_compute3 = 5.U
  val s_compute4 = 6.U
  val s_compute5 = 7.U
  val s_compute6 = 8.U

  val sw_compute0 = 0.U
  val sw_compute1 = 1.U
  val sw_compute2 = 2.U
  val sw_wait = 3.U
  val r_wstate = Reg(init=sw_compute0)

  val rk_unit = Module(new LVRK2_7Stage(fpu))
  rk_set_inputs(rk_unit)

  switch(r_state) {
    is(s_write) {
      sf_write_spfp(s_next=s_compute3)

      switch(r_wstate) {
        is(sw_compute0) {
          rk_unit.io.stage := 0.U
          r_wstate := sw_compute1
        }
        is(sw_compute1) {
          rk_unit.io.stage := 1.U
          r_wstate := sw_compute2
        }
        is(sw_compute2) {
          rk_unit.io.stage := 2.U
          r_wstate := sw_wait
        }
        is(sw_wait) {
          rk_unit.io.stage := 7.U //inhibit stage
        }
      }
    }
    is(s_compute3) {
      rk_unit.io.stage := 3.U
      r_state := s_compute4
    }
    is(s_compute4) {
      rk_unit.io.stage := 4.U
      r_state := s_compute5
    }
    is(s_compute5) {
      rk_unit.io.stage := 5.U
      r_state := s_compute6
    }
    is(s_compute6) {
      rk_unit.io.stage := 6.U
      r_x := rk_unit.io.x_np1
      r_y := rk_unit.io.y_np1
      r_state := s_write
      r_wstate := sw_compute0
    }
  }
}

class AccelConfRK210sP(outer: DiffEqAccel) extends CommonSpfp(outer) {
  val s_compute3 = 5.U
  val s_compute4 = 6.U
  val s_compute5 = 7.U
  val s_compute6 = 8.U
  val s_compute7 = 9.U
  val s_compute8 = 10.U
  val s_compute9 = 11.U

  val sw_compute0 = 0.U
  val sw_compute1 = 1.U
  val sw_compute2 = 2.U
  val sw_wait = 3.U
  val r_wstate = Reg(init=sw_compute0)

  val rk_unit = Module(new LVRK2_10Stage(fpu))
  rk_set_inputs(rk_unit)

  switch(r_state) {
    is(s_write) {
      sf_write_spfp(s_next=s_compute3)

      switch(r_wstate) {
        is(sw_compute0) {
          rk_unit.io.stage := 0.U
          r_wstate := sw_compute1
        }
        is(sw_compute1) {
          rk_unit.io.stage := 1.U
          r_wstate := sw_compute2
        }
        is(sw_compute2) {
          rk_unit.io.stage := 2.U
          r_wstate := sw_wait
        }
        is(sw_wait) {
          rk_unit.io.stage := 10.U //inhibit stage
        }
      }
    }
    is(s_compute3) {
      rk_unit.io.stage := 3.U
      r_state := s_compute4
    }
    is(s_compute4) {
      rk_unit.io.stage := 4.U
      r_state := s_compute5
    }
    is(s_compute5) {
      rk_unit.io.stage := 5.U
      r_state := s_compute6
    }
    is(s_compute6) {
      rk_unit.io.stage := 6.U
      r_state := s_compute7
    }
    is(s_compute7) {
      rk_unit.io.stage := 7.U
      r_state := s_compute8
    }
    is(s_compute8) {
      rk_unit.io.stage := 8.U
      r_state := s_compute9
    }
    is(s_compute9) {
      rk_unit.io.stage := 9.U
      r_x := rk_unit.io.x_np1
      r_y := rk_unit.io.y_np1
      r_state := s_write
      r_wstate := sw_compute0
    }
  }
}

class AccelConfRK220sP(outer: DiffEqAccel) extends CommonSpfp(outer) {
  val s_compute3 = 5.U
  val s_compute4 = 6.U
  val s_compute5 = 7.U
  val s_compute6 = 8.U
  val s_compute7 = 9.U
  val s_compute8 = 10.U
  val s_compute9 = 11.U
  val s_compute10 = 12.U
  val s_compute11 = 13.U
  val s_compute12 = 14.U
  val s_compute13 = 15.U
  val s_compute14 = 16.U
  val s_compute15 = 17.U
  val s_compute16 = 18.U
  val s_compute17 = 19.U
  val s_compute18 = 20.U
  val s_compute19 = 21.U

  val sw_compute0 = 0.U
  val sw_compute1 = 1.U
  val sw_compute2 = 2.U
  val sw_wait = 3.U
  val r_wstate = Reg(init=sw_compute0)

  val rk_unit = Module(new LVRK2_20Stage(fpu))
  rk_set_inputs(rk_unit)

  switch(r_state) {
    is(s_write) {
      sf_write_spfp(s_next=s_compute3)

      switch(r_wstate) {
        is(sw_compute0) {
          rk_unit.io.stage := 0.U
          r_wstate := sw_compute1
        }
        is(sw_compute1) {
          rk_unit.io.stage := 1.U
          r_wstate := sw_compute2
        }
        is(sw_compute2) {
          rk_unit.io.stage := 2.U
          r_wstate := sw_wait
        }
        is(sw_wait) {
          rk_unit.io.stage := 20.U //inhibit stage
        }
      }
    }
    is(s_compute3) {
      rk_unit.io.stage := 3.U
      r_state := s_compute4
    }
    is(s_compute4) {
      rk_unit.io.stage := 4.U
      r_state := s_compute5
    }
    is(s_compute5) {
      rk_unit.io.stage := 5.U
      r_state := s_compute6
    }
    is(s_compute6) {
      rk_unit.io.stage := 6.U
      r_state := s_compute7
    }
    is(s_compute7) {
      rk_unit.io.stage := 7.U
      r_state := s_compute8
    }
    is(s_compute8) {
      rk_unit.io.stage := 8.U
      r_state := s_compute9
    }
    is(s_compute9) {
      rk_unit.io.stage := 9.U
      r_state := s_compute10
    }
    is(s_compute10) {
      rk_unit.io.stage := 10.U
      r_state := s_compute11
    }
    is(s_compute11) {
      rk_unit.io.stage := 11.U
      r_state := s_compute12
    }
    is(s_compute12) {
      rk_unit.io.stage := 12.U
      r_state := s_compute13
    }
    is(s_compute13) {
      rk_unit.io.stage := 13.U
      r_state := s_compute14
    }
    is(s_compute14) {
      rk_unit.io.stage := 14.U
      r_state := s_compute15
    }
    is(s_compute15) {
      rk_unit.io.stage := 15.U
      r_state := s_compute16
    }
    is(s_compute16) {
      rk_unit.io.stage := 16.U
      r_state := s_compute17
    }
    is(s_compute17) {
      rk_unit.io.stage := 17.U
      r_state := s_compute18
    }
    is(s_compute18) {
      rk_unit.io.stage := 18.U
      r_x := rk_unit.io.x_np1
      r_state := s_compute19
    }
    is(s_compute19) {
      rk_unit.io.stage := 19.U
      r_y := rk_unit.io.y_np1
      r_state := s_write
      r_wstate := sw_compute0
    }
  }
}

