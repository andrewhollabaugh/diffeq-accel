package diffeq

import Chisel._
import hardfloat._

case class FPU(use_double: Boolean) {
  val w_exp = if(use_double) 11 else 8
  val w_sig = if(use_double) 53 else 24
  val w_total = (w_exp + w_sig).W
  val w_total_rec = (w_exp + w_sig + 1).W

  def fp_to_recfp(normal_in: Bits): Bits = {
    recFNFromFN(w_exp, w_sig, normal_in)
  }
  
  def recfp_to_fp(recoded_in: Bits): Bits = {
    fNFromRecFN(w_exp, w_sig, recoded_in)
  }

  def int_to_recfp(in: UInt): Bits = {
    val hf_int_to_recfp = Module(new INToRecFN(1, w_exp, w_sig))
    hf_int_to_recfp.io.signedIn := false.B
    hf_int_to_recfp.io.in := in
    hf_int_to_recfp.io.roundingMode := consts.round_near_even
    hf_int_to_recfp.io.detectTininess := consts.tininess_afterRounding
    hf_int_to_recfp.io.out
  }

  def recfp_to_int(in: Bits, int_width: Int): Bits = {
    val hf_recfp_to_int = Module(new RecFNToIN(w_exp, w_sig, int_width))
    hf_recfp_to_int.io.in := in
    hf_recfp_to_int.io.roundingMode := consts.round_near_even
    hf_recfp_to_int.io.signedOut := false.B
    hf_recfp_to_int.io.out
  }

  def mul_add_single(
    mul1: Option[Bits] = None, 
    mul2: Option[Bits] = None, 
    addend: Option[Bits] = None
  ): Bits = {
    val mau = new MulAddArray(fpu=this, num_units=1)
    mau.mul_add(i=0, mul1=mul1, mul2=mul2, addend=addend)
  }
}

class MulAddArray(fpu: FPU, num_units: Int) {
  val ma_units = for(i <- 0 until num_units) yield {
    val hf_mul_adder = Module(new MulAddRecFN(fpu.w_exp, fpu.w_sig))
    hf_mul_adder.io.op := 0.U
    hf_mul_adder.io.roundingMode := consts.round_near_even
    hf_mul_adder.io.detectTininess := consts.tininess_afterRounding
    hf_mul_adder
  }
  val ma_units_io = Vec(ma_units.map(_.io))

  def mul_add(
    i: Int, //index (address) 
    mul1: Option[Bits] = None, 
    mul2: Option[Bits] = None, 
    addend: Option[Bits] = None
  ): Bits = {
    val hf_mul_adder_io = ma_units_io(i)
    hf_mul_adder_io.a := mul1.getOrElse(fpu.int_to_recfp(1.U))
    hf_mul_adder_io.b := mul2.getOrElse(fpu.int_to_recfp(1.U))
    hf_mul_adder_io.c := addend.getOrElse(fpu.int_to_recfp(0.U))
    hf_mul_adder_io.out
  }
}

