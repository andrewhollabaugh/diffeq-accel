package diffeq

import Chisel._

class LVRK(fpu: FPU) extends Module {
  val io = IO(new Bundle {
    val stage = UInt(INPUT)
    val h = Bits(INPUT)
    val x = Bits(INPUT)
    val y = Bits(INPUT)
    val co_lambda = Bits(INPUT)
    val co_neg_b = Bits(INPUT)
    val co_neg_mu = Bits(INPUT)
    val co_c = Bits(INPUT)
    val x_np1 = Bits(OUTPUT)
    val y_np1 = Bits(OUTPUT)
  })

  val float_width = fpu.w_total_rec

  val h_r = fpu.fp_to_recfp(io.h)
  val x_r = fpu.fp_to_recfp(io.x)
  val y_r = fpu.fp_to_recfp(io.y)
  val co_lambda_r = fpu.fp_to_recfp(io.co_lambda)
  val co_neg_b_r = fpu.fp_to_recfp(io.co_neg_b)
  val co_neg_mu_r = fpu.fp_to_recfp(io.co_neg_mu)
  val co_c_r = fpu.fp_to_recfp(io.co_c)

  val half_r = if(fpu.use_double) {
    fpu.fp_to_recfp("h_3fe0_0000_0000_0000".U) 
  } else {
    fpu.fp_to_recfp("h_3f00_0000".U)
  }
}

class LVRK4(fpu: FPU) extends LVRK(fpu) {
  val sixth_r = if(fpu.use_double) {
    fpu.fp_to_recfp("h_3fc5_5555_5555_5555".U)
  } else {
    fpu.fp_to_recfp("h_3e2a_aaab".U)
  }

  val two_r = if(fpu.use_double) {
    fpu.fp_to_recfp("h_4000_0000_0000_0000".U)
  } else {
    fpu.fp_to_recfp("h_4000_0000".U)
  }
}

class LVRK2_20Stage(fpu: FPU) extends LVRK(fpu) {
  val maus = new MulAddArray(fpu, num_units=1)

  val r_kx1 = Reg(Bits(float_width))
  val r_ky1 = Reg(Bits(float_width))
  val r_kx2 = Reg(Bits(float_width))
  val r_ky2 = Reg(Bits(float_width))
  val r_interim0 = Reg(Bits(float_width))
  val r_interim1 = Reg(Bits(float_width))

  switch(io.stage) {
    is(0.U) {
      val kx1_i0 = maus.mul_add(i=0, mul1=Some(co_neg_b_r), mul2=Some(y_r), addend=Some(co_lambda_r))
      r_interim0 := kx1_i0
    }
    is(1.U) {
      val ky1_i0 = maus.mul_add(i=0, mul1=Some(co_c_r), mul2=Some(x_r), addend=Some(co_neg_mu_r))
      r_interim1 := ky1_i0
    }
    is(2.U) {
      val kx1_i0 = r_interim0
      val kx1_i1 = maus.mul_add(i=0, mul1=Some(kx1_i0), mul2=Some(x_r))
      r_interim0 := kx1_i1
    }
    is(3.U) {
      val ky1_i0 = r_interim1
      val ky1_i1 = maus.mul_add(i=0, mul1=Some(ky1_i0), mul2=Some(y_r))
      r_interim1 := ky1_i1
    }
    is(4.U) {
      val kx1_i1 = r_interim0
      val kx1 = maus.mul_add(i=0, mul1=Some(kx1_i1), mul2=Some(h_r))
      r_kx1 := kx1
    }
    is(5.U) {
      val ky1_i1 = r_interim1
      val ky1 = maus.mul_add(i=0, mul1=Some(ky1_i1), mul2=Some(h_r))
      r_ky1 := ky1
    }
    is(6.U) {
      val kx1 = r_kx1
      val kx2_i00 = maus.mul_add(i=0, mul1=Some(kx1), addend=Some(x_r))
      r_kx2 := kx2_i00
    }
    is(7.U) {
      val ky1 = r_ky1
      val kx2_i10 = maus.mul_add(i=0, mul1=Some(ky1), addend=Some(y_r))
      r_interim0 := kx2_i10
    }
    is(8.U) {
      val ky1 = r_ky1
      val ky2_i00 = maus.mul_add(i=0, mul1=Some(ky1), addend=Some(y_r))
      r_ky2 := ky2_i00
    }
    is(9.U) {
      val kx1 = r_kx1
      val ky2_i10 = maus.mul_add(i=0, mul1=Some(kx1), addend=Some(x_r))
      r_interim1 := ky2_i10
    }
    is(10.U) {
      val kx2_i00 = r_kx2
      val kx2_i01 = maus.mul_add(i=0, mul1=Some(kx2_i00), mul2=Some(h_r))
      r_kx2 := kx2_i01
    }
    is(11.U) {
      val kx2_i10 = r_interim0
      val kx2_i11 = maus.mul_add(i=0, mul1=Some(kx2_i10), mul2=Some(co_neg_b_r), addend=Some(co_lambda_r))
      r_interim0 := kx2_i11
    }
    is(12.U) {
      val ky2_i00 = r_ky2
      val ky2_i01 = maus.mul_add(i=0, mul1=Some(ky2_i00), mul2=Some(h_r))
      r_ky2 := ky2_i01
    }
    is(13.U) {
      val ky2_i10 = r_interim1
      val ky2_i11 = maus.mul_add(i=0, mul1=Some(ky2_i10), mul2=Some(co_c_r), addend=Some(co_neg_mu_r))
      r_interim1 := ky2_i11
    }
    is(14.U) {
      val kx2_i01 = r_kx2
      val kx2_i11 = r_interim0
      val kx2 = maus.mul_add(i=0, mul1=Some(kx2_i01), mul2=Some(kx2_i11))
      r_kx2 := kx2
    }
    is(15.U) {
      val ky2_i01 = r_ky2
      val ky2_i11 = r_interim1
      val ky2 = maus.mul_add(i=0, mul1=Some(ky2_i01), mul2=Some(ky2_i11))
      r_ky2 := ky2
    }
    is(16.U) {
      val kx1 = r_kx1
      val kx2 = r_kx2
      val x_np1_i0 = maus.mul_add(i=0, mul1=Some(kx1), addend=Some(kx2))
      r_interim0 := x_np1_i0
    }
    is(17.U) {
      val ky1 = r_ky1
      val ky2 = r_ky2
      val y_np1_i0 = maus.mul_add(i=0, mul1=Some(ky1), addend=Some(ky2))
      r_interim1 := y_np1_i0
    }
    is(18.U) {
      val x_np1_i0 = r_interim0
      val x_np1_r = maus.mul_add(i=0, mul1=Some(x_np1_i0), mul2=Some(half_r), addend=Some(x_r))
      io.x_np1 := fpu.recfp_to_fp(x_np1_r)
    }
    is(19.U) {
      val y_np1_i0 = r_interim1
      val y_np1_r = maus.mul_add(i=0, mul1=Some(y_np1_i0), mul2=Some(half_r), addend=Some(y_r))
      io.y_np1 := fpu.recfp_to_fp(y_np1_r)
    }
    is(20.U) { } //inhibit stage for piplining
  }
}


class LVRK2_10Stage(fpu: FPU) extends LVRK(fpu) {
  val maus = new MulAddArray(fpu, num_units=2)

  val r_kx1 = Reg(Bits(float_width))
  val r_ky1 = Reg(Bits(float_width))
  val r_kx2 = Reg(Bits(float_width))
  val r_ky2 = Reg(Bits(float_width))
  val r_interim0 = Reg(Bits(float_width))
  val r_interim1 = Reg(Bits(float_width))

  switch(io.stage) {
    is(0.U) {
      val kx1_i0 = maus.mul_add(i=0, mul1=Some(co_neg_b_r), mul2=Some(y_r), addend=Some(co_lambda_r))
      val ky1_i0 = maus.mul_add(i=1, mul1=Some(co_c_r), mul2=Some(x_r), addend=Some(co_neg_mu_r))
      r_interim0 := kx1_i0
      r_interim1 := ky1_i0
    }
    is(1.U) {
      val kx1_i0 = r_interim0
      val ky1_i0 = r_interim1
      val kx1_i1 = maus.mul_add(i=0, mul1=Some(kx1_i0), mul2=Some(x_r))
      val ky1_i1 = maus.mul_add(i=1, mul1=Some(ky1_i0), mul2=Some(y_r))
      r_interim0 := kx1_i1
      r_interim1 := ky1_i1
    }
    is(2.U) {
      val kx1_i1 = r_interim0
      val ky1_i1 = r_interim1
      val kx1 = maus.mul_add(i=0, mul1=Some(kx1_i1), mul2=Some(h_r))
      val ky1 = maus.mul_add(i=1, mul1=Some(ky1_i1), mul2=Some(h_r))
      r_kx1 := kx1
      r_ky1 := ky1
    }
    is(3.U) {
      val kx1 = r_kx1
      val ky1 = r_ky1
      val kx2_i00 = maus.mul_add(i=0, mul1=Some(kx1), addend=Some(x_r))
      val kx2_i10 = maus.mul_add(i=1, mul1=Some(ky1), addend=Some(y_r))
      r_kx2 := kx2_i00
      r_interim0 := kx2_i10
    }
    is(4.U) {
      val kx1 = r_kx1
      val ky1 = r_ky1
      val ky2_i00 = maus.mul_add(i=0, mul1=Some(ky1), addend=Some(y_r))
      val ky2_i10 = maus.mul_add(i=1, mul1=Some(kx1), addend=Some(x_r))
      r_ky2 := ky2_i00
      r_interim1 := ky2_i10
    }
    is(5.U) {
      val kx2_i00 = r_kx2
      val kx2_i10 = r_interim0
      val kx2_i01 = maus.mul_add(i=0, mul1=Some(kx2_i00), mul2=Some(h_r))
      val kx2_i11 = maus.mul_add(i=1, mul1=Some(kx2_i10), mul2=Some(co_neg_b_r), addend=Some(co_lambda_r))
      r_kx2 := kx2_i01
      r_interim0 := kx2_i11
    }
    is(6.U) {
      val ky2_i00 = r_ky2
      val ky2_i10 = r_interim1
      val ky2_i01 = maus.mul_add(i=0, mul1=Some(ky2_i00), mul2=Some(h_r))
      val ky2_i11 = maus.mul_add(i=1, mul1=Some(ky2_i10), mul2=Some(co_c_r), addend=Some(co_neg_mu_r))
      r_ky2 := ky2_i01
      r_interim1 := ky2_i11
    }
    is(7.U) {
      val kx2_i01 = r_kx2
      val kx2_i11 = r_interim0
      val ky2_i01 = r_ky2
      val ky2_i11 = r_interim1
      val kx2 = maus.mul_add(i=0, mul1=Some(kx2_i01), mul2=Some(kx2_i11))
      val ky2 = maus.mul_add(i=1, mul1=Some(ky2_i01), mul2=Some(ky2_i11))
      r_kx2 := kx2
      r_ky2 := ky2
    }
    is(8.U) {
      val kx1 = r_kx1
      val ky1 = r_ky1
      val kx2 = r_kx2
      val ky2 = r_ky2
      val x_np1_i0 = maus.mul_add(i=0, mul1=Some(kx1), addend=Some(kx2))
      val y_np1_i0 = maus.mul_add(i=1, mul1=Some(ky1), addend=Some(ky2))
      r_interim0 := x_np1_i0
      r_interim1 := y_np1_i0
    }
    is(9.U) {
      val x_np1_i0 = r_interim0
      val y_np1_i0 = r_interim1
      val x_np1_r = maus.mul_add(i=0, mul1=Some(x_np1_i0), mul2=Some(half_r), addend=Some(x_r))
      val y_np1_r = maus.mul_add(i=1, mul1=Some(y_np1_i0), mul2=Some(half_r), addend=Some(y_r))
      io.x_np1 := fpu.recfp_to_fp(x_np1_r)
      io.y_np1 := fpu.recfp_to_fp(y_np1_r)
    }
    is(10.U) { } //inhibit stage for piplining
  }
}

class LVRK2_7Stage(fpu: FPU) extends LVRK(fpu) {
  val maus = new MulAddArray(fpu, num_units=3)

  val r_kx1 = Reg(Bits(float_width))
  val r_ky1 = Reg(Bits(float_width))
  val r_kx2 = Reg(Bits(float_width))
  val r_ky2 = Reg(Bits(float_width))
  val r_interim0 = Reg(Bits(float_width))
  val r_interim1 = Reg(Bits(float_width))

  switch(io.stage) {
    is(0.U) {
      val kx1_i0 = maus.mul_add(i=0, mul1=Some(co_neg_b_r), mul2=Some(y_r), addend=Some(co_lambda_r))
      val kx1_i1 = maus.mul_add(i=1, mul1=Some(kx1_i0), mul2=Some(x_r))
      val ky1_i0 = maus.mul_add(i=2, mul1=Some(co_c_r), mul2=Some(x_r), addend=Some(co_neg_mu_r))
      r_kx1 := kx1_i1
      r_ky1 := ky1_i0
    }
    is(1.U) {
      val kx1_i1 = r_kx1
      val ky1_i0 = r_ky1
      val kx1 = maus.mul_add(i=0, mul1=Some(kx1_i1), mul2=Some(h_r))
      val ky1_i1 = maus.mul_add(i=1, mul1=Some(ky1_i0), mul2=Some(y_r))
      val ky1 = maus.mul_add(i=2, mul1=Some(ky1_i1), mul2=Some(h_r))
      r_kx1 := kx1
      r_ky1 := ky1
    }
    is(2.U) {
      val kx1 = r_kx1
      val ky1 = r_ky1
      val kx2_i00 = maus.mul_add(i=0, mul1=Some(kx1), addend=Some(x_r))
      val kx2_i01 = maus.mul_add(i=1, mul1=Some(kx2_i00), mul2=Some(h_r))
      val kx2_i10 = maus.mul_add(i=2, mul1=Some(ky1), addend=Some(y_r))
      r_kx2 := kx2_i01
      r_interim0 := kx2_i10
    }
    is(3.U) {
      val ky1 = r_ky1
      val kx2_i01 = r_kx2
      val kx2_i10 = r_interim0
      val kx2_i11 = maus.mul_add(i=0, mul1=Some(kx2_i10), mul2=Some(co_neg_b_r), addend=Some(co_lambda_r))
      val kx2 = maus.mul_add(i=1, mul1=Some(kx2_i01), mul2=Some(kx2_i11))
      val ky2_i00 = maus.mul_add(i=2, mul1=Some(ky1), addend=Some(y_r))
      r_kx2 := kx2
      r_ky2 := ky2_i00
    }
    is(4.U) {
      val kx1 = r_kx1
      val ky2_i00 = r_ky2
      val ky2_i01 = maus.mul_add(i=0, mul1=Some(ky2_i00), mul2=Some(h_r))
      val ky2_i10 = maus.mul_add(i=1, mul1=Some(kx1), addend=Some(x_r))
      val ky2_i11 = maus.mul_add(i=2, mul1=Some(ky2_i10), mul2=Some(co_c_r), addend=Some(co_neg_mu_r))
      r_ky2 := ky2_i01
      r_interim1 := ky2_i11
    }
    is(5.U) {
      val ky2_i01 = r_ky2
      val ky2_i11 = r_interim1
      val kx1 = r_kx1
      val ky1 = r_ky1
      val kx2 = r_kx2
      val ky2 = maus.mul_add(i=0, mul1=Some(ky2_i01), mul2=Some(ky2_i11))
      val x_np1_i0 = maus.mul_add(i=1, mul1=Some(kx1), addend=Some(kx2))
      val y_np1_i0 = maus.mul_add(i=2, mul1=Some(ky1), addend=Some(ky2))
      r_interim0 := x_np1_i0
      r_interim1 := y_np1_i0
    }
    is(6.U) {
      val x_np1_i0 = r_interim0
      val y_np1_i0 = r_interim1
      val x_np1_r = maus.mul_add(i=0, mul1=Some(x_np1_i0), mul2=Some(half_r), addend=Some(x_r))
      val y_np1_r = maus.mul_add(i=1, mul1=Some(y_np1_i0), mul2=Some(half_r), addend=Some(y_r))
      io.x_np1 := fpu.recfp_to_fp(x_np1_r)
      io.y_np1 := fpu.recfp_to_fp(y_np1_r)
    }
    is(7.U) { } //inhibit stage for piplining
  }
}

class LVRK2_5Stage(fpu: FPU) extends LVRK(fpu) {
  val maus = new MulAddArray(fpu, num_units=4)

  val r_kx1 = Reg(Bits(float_width))
  val r_ky1 = Reg(Bits(float_width))
  val r_kx2 = Reg(Bits(float_width))
  val r_ky2 = Reg(Bits(float_width))
  val r_interim0 = Reg(Bits(float_width))
  val r_interim1 = Reg(Bits(float_width))

  switch(io.stage) {
    is(0.U) {
      val kx1_i0 = maus.mul_add(i=0, mul1=Some(co_neg_b_r), mul2=Some(y_r), addend=Some(co_lambda_r))
      val kx1_i1 = maus.mul_add(i=1, mul1=Some(kx1_i0), mul2=Some(x_r))
      val ky1_i0 = maus.mul_add(i=2, mul1=Some(co_c_r), mul2=Some(x_r), addend=Some(co_neg_mu_r))
      val ky1_i1 = maus.mul_add(i=3, mul1=Some(ky1_i0), mul2=Some(y_r))
      r_kx1 := kx1_i1
      r_ky1 := ky1_i1
    }
    is(1.U) {
      val kx1_i1 = r_kx1
      val ky1_i1 = r_ky1
      val kx1 = maus.mul_add(i=0, mul1=Some(kx1_i1), mul2=Some(h_r))
      val ky1 = maus.mul_add(i=1, mul1=Some(ky1_i1), mul2=Some(h_r))
      val kx2_i00 = maus.mul_add(i=2, mul1=Some(kx1), addend=Some(x_r))
      val ky2_i00 = maus.mul_add(i=3, mul1=Some(ky1), addend=Some(y_r))
      r_kx1 := kx1
      r_ky1 := ky1
      r_kx2 := kx2_i00
      r_ky2 := ky2_i00
    }
    is(2.U) {
      val kx2_i00 = r_kx2
      val ky2_i00 = r_ky2
      val kx1 = r_kx1
      val ky1 = r_ky1
      val kx2_i01 = maus.mul_add(i=0, mul1=Some(kx2_i00), mul2=Some(h_r))
      val kx2_i10 = maus.mul_add(i=1, mul1=Some(ky1), addend=Some(y_r))
      val ky2_i01 = maus.mul_add(i=2, mul1=Some(ky2_i00), mul2=Some(h_r))
      val ky2_i10 = maus.mul_add(i=3, mul1=Some(kx1), addend=Some(x_r))
      r_kx2 := kx2_i01
      r_interim0 := kx2_i10
      r_ky2 := ky2_i01
      r_interim1 := ky2_i10
    }
    is(3.U) {
      val kx2_i01 = r_kx2
      val kx2_i10 = r_interim0
      val ky2_i01 = r_ky2
      val ky2_i10 = r_interim1
      val kx2_i11 = maus.mul_add(i=0, mul1=Some(kx2_i10), mul2=Some(co_neg_b_r), addend=Some(co_lambda_r))
      val ky2_i11 = maus.mul_add(i=1, mul1=Some(ky2_i10), mul2=Some(co_c_r), addend=Some(co_neg_mu_r))
      val kx2 = maus.mul_add(i=2, mul1=Some(kx2_i01), mul2=Some(kx2_i11))
      val ky2 = maus.mul_add(i=3, mul1=Some(ky2_i01), mul2=Some(ky2_i11))
      r_kx2 := kx2
      r_ky2 := ky2
    }
    is(4.U) {
      val kx1 = r_kx1
      val ky1 = r_ky1
      val kx2 = r_kx2
      val ky2 = r_ky2
      val x_np1_i0 = maus.mul_add(i=0, mul1=Some(kx1), addend=Some(kx2))
      val y_np1_i0 = maus.mul_add(i=1, mul1=Some(ky1), addend=Some(ky2))
      val x_np1_r = maus.mul_add(i=2, mul1=Some(x_np1_i0), mul2=Some(half_r), addend=Some(x_r))
      val y_np1_r = maus.mul_add(i=3, mul1=Some(y_np1_i0), mul2=Some(half_r), addend=Some(y_r))
      io.x_np1 := fpu.recfp_to_fp(x_np1_r)
      io.y_np1 := fpu.recfp_to_fp(y_np1_r)
    }
    is(5.U) { } //inhibit stage for piplining
  }
}

class LVRK2_4Stage(fpu: FPU) extends LVRK(fpu) {
  val maus = new MulAddArray(fpu, num_units=5)

  val r_kx1 = Reg(Bits(float_width))
  val r_ky1 = Reg(Bits(float_width))
  val r_kx2 = Reg(Bits(float_width))
  val r_interim0 = Reg(Bits(float_width))
  val r_interim1 = Reg(Bits(float_width))

  switch(io.stage) {
    is(0.U) {
      val kx1_i0 = maus.mul_add(i=0, mul1=Some(co_neg_b_r), mul2=Some(y_r), addend=Some(co_lambda_r))
      val kx1_i1 = maus.mul_add(i=1, mul1=Some(kx1_i0), mul2=Some(x_r))
      val kx1 = maus.mul_add(i=2, mul1=Some(kx1_i1), mul2=Some(h_r))

      val ky1_i0 = maus.mul_add(i=3, mul1=Some(co_c_r), mul2=Some(x_r), addend=Some(co_neg_mu_r))
      val ky1_i1 = maus.mul_add(i=4, mul1=Some(ky1_i0), mul2=Some(y_r))

      r_kx1 := kx1
      r_interim0 := ky1_i1
    }
    is(1.U) {
      val kx1 = r_kx1
      val ky1_i1 = r_interim0

      val ky1 = maus.mul_add(i=0, mul1=Some(ky1_i1), mul2=Some(h_r))

      val kx2_i00 = maus.mul_add(i=1, mul1=Some(kx1), addend=Some(x_r))
      val kx2_i01 = maus.mul_add(i=2, mul1=Some(kx2_i00), mul2=Some(h_r))
      val kx2_i10 = maus.mul_add(i=3, mul1=Some(ky1), addend=Some(y_r))
      val kx2_i11 = maus.mul_add(i=4, mul1=Some(kx2_i10), mul2=Some(co_neg_b_r), addend=Some(co_lambda_r))

      r_ky1 := ky1
      r_interim0 := kx2_i01
      r_interim1 := kx2_i11
    }
    is(2.U) {
      val kx2_i01 = r_interim0
      val kx2_i11 = r_interim1
      val ky1 = r_ky1
      val kx1 = r_kx1

      val kx2 = maus.mul_add(i=0, mul1=Some(kx2_i01), mul2=Some(kx2_i11))

      val ky2_i00 = maus.mul_add(i=1, mul1=Some(ky1), addend=Some(y_r))
      val ky2_i01 = maus.mul_add(i=2, mul1=Some(ky2_i00), mul2=Some(h_r))
      val ky2_i10 = maus.mul_add(i=3, mul1=Some(kx1), addend=Some(x_r))
      val ky2_i11 = maus.mul_add(i=4, mul1=Some(ky2_i10), mul2=Some(co_c_r), addend=Some(co_neg_mu_r))

      r_kx2 := kx2
      r_interim0 := ky2_i01
      r_interim1 := ky2_i11
    }
    is(3.U) {
      val ky2_i01 = r_interim0
      val ky2_i11 = r_interim1
      val kx1 = r_kx1
      val ky1 = r_ky1
      val kx2 = r_kx2

      val ky2 = maus.mul_add(i=0, mul1=Some(ky2_i01), mul2=Some(ky2_i11))

      val x_np1_i0 = maus.mul_add(i=1, mul1=Some(kx1), addend=Some(kx2))
      val x_np1_r = maus.mul_add(i=2, mul1=Some(x_np1_i0), mul2=Some(half_r), addend=Some(x_r))

      val y_np1_i0 = maus.mul_add(i=3, mul1=Some(ky1), addend=Some(ky2))
      val y_np1_r = maus.mul_add(i=4, mul1=Some(y_np1_i0), mul2=Some(half_r), addend=Some(y_r))

      io.x_np1 := fpu.recfp_to_fp(x_np1_r)
      io.y_np1 := fpu.recfp_to_fp(y_np1_r)
    }
    is(4.U) { } //inhibit stage for piplining
  }
}

class LVRK2_2Stage(fpu: FPU) extends LVRK(fpu) {
  val maus = new MulAddArray(fpu, num_units=10)

  val r_kx1 = Reg(Bits(float_width))
  val r_ky1 = Reg(Bits(float_width))
  val r_interim0 = Reg(Bits(float_width))
  val r_interim1 = Reg(Bits(float_width))

  switch(io.stage) {
    is(0.U) {
      val kx1_i0 = maus.mul_add(i=0, mul1=Some(co_neg_b_r), mul2=Some(y_r), addend=Some(co_lambda_r))
      val kx1_i1 = maus.mul_add(i=1, mul1=Some(kx1_i0), mul2=Some(x_r))
      val kx1 = maus.mul_add(i=2, mul1=Some(kx1_i1), mul2=Some(h_r))

      val ky1_i0 = maus.mul_add(i=3, mul1=Some(co_c_r), mul2=Some(x_r), addend=Some(co_neg_mu_r))
      val ky1_i1 = maus.mul_add(i=4, mul1=Some(ky1_i0), mul2=Some(y_r))
      val ky1 = maus.mul_add(i=5, mul1=Some(ky1_i1), mul2=Some(h_r))

      val kx2_i00 = maus.mul_add(i=6, mul1=Some(kx1), addend=Some(x_r))
      val kx2_i01 = maus.mul_add(i=7, mul1=Some(kx2_i00), mul2=Some(h_r))
      val kx2_i10 = maus.mul_add(i=8, mul1=Some(ky1), addend=Some(y_r))
      val kx2_i11 = maus.mul_add(i=9, mul1=Some(kx2_i10), mul2=Some(co_neg_b_r), addend=Some(co_lambda_r))

      r_kx1 := kx1
      r_ky1 := ky1
      r_interim0 := kx2_i01
      r_interim1 := kx2_i11
    }
    is(1.U) {
      val kx1 = r_kx1
      val ky1 = r_ky1
      val kx2_i01 = r_interim0
      val kx2_i11 = r_interim1

      val kx2 = maus.mul_add(i=0, mul1=Some(kx2_i01), mul2=Some(kx2_i11))

      val ky2_i00 = maus.mul_add(i=1, mul1=Some(ky1), addend=Some(y_r))
      val ky2_i01 = maus.mul_add(i=2, mul1=Some(ky2_i00), mul2=Some(h_r))
      val ky2_i10 = maus.mul_add(i=3, mul1=Some(kx1), addend=Some(x_r))
      val ky2_i11 = maus.mul_add(i=4, mul1=Some(ky2_i10), mul2=Some(co_c_r), addend=Some(co_neg_mu_r))
      val ky2 = maus.mul_add(i=5, mul1=Some(ky2_i01), mul2=Some(ky2_i11))

      val x_np1_i0 = maus.mul_add(i=6, mul1=Some(kx1), addend=Some(kx2))
      val x_np1_r = maus.mul_add(i=7, mul1=Some(x_np1_i0), mul2=Some(half_r), addend=Some(x_r))

      val y_np1_i0 = maus.mul_add(i=8, mul1=Some(ky1), addend=Some(ky2))
      val y_np1_r = maus.mul_add(i=9, mul1=Some(y_np1_i0), mul2=Some(half_r), addend=Some(y_r))

      io.x_np1 := fpu.recfp_to_fp(x_np1_r)
      io.y_np1 := fpu.recfp_to_fp(y_np1_r)
    }
  }
}

class LVRK2_1Stage(fpu: FPU) extends LVRK(fpu) {
  val kx1_i0 = fpu.mul_add_single(mul1=Some(co_neg_b_r), mul2=Some(y_r), addend=Some(co_lambda_r))
  val kx1_i1 = fpu.mul_add_single(mul1=Some(kx1_i0), mul2=Some(x_r))
  val kx1 = fpu.mul_add_single(mul1=Some(kx1_i1), mul2=Some(h_r))

  val ky1_i0 = fpu.mul_add_single(mul1=Some(co_c_r), mul2=Some(x_r), addend=Some(co_neg_mu_r))
  val ky1_i1 = fpu.mul_add_single(mul1=Some(ky1_i0), mul2=Some(y_r))
  val ky1 = fpu.mul_add_single(mul1=Some(ky1_i1), mul2=Some(h_r))

  val kx2_i00 = fpu.mul_add_single(mul1=Some(kx1), addend=Some(x_r))
  val kx2_i01 = fpu.mul_add_single(mul1=Some(kx2_i00), mul2=Some(h_r))
  val kx2_i10 = fpu.mul_add_single(mul1=Some(ky1), addend=Some(y_r))
  val kx2_i11 = fpu.mul_add_single(mul1=Some(kx2_i10), mul2=Some(co_neg_b_r), addend=Some(co_lambda_r))
  val kx2 = fpu.mul_add_single(mul1=Some(kx2_i01), mul2=Some(kx2_i11))

  val ky2_i00 = fpu.mul_add_single(mul1=Some(ky1), addend=Some(y_r))
  val ky2_i01 = fpu.mul_add_single(mul1=Some(ky2_i00), mul2=Some(h_r))
  val ky2_i10 = fpu.mul_add_single(mul1=Some(kx1), addend=Some(x_r))
  val ky2_i11 = fpu.mul_add_single(mul1=Some(ky2_i10), mul2=Some(co_c_r), addend=Some(co_neg_mu_r))
  val ky2 = fpu.mul_add_single(mul1=Some(ky2_i01), mul2=Some(ky2_i11))

  val x_np1_i0 = fpu.mul_add_single(mul1=Some(kx1), addend=Some(kx2))
  val x_np1_r = fpu.mul_add_single(mul1=Some(x_np1_i0), mul2=Some(half_r), addend=Some(x_r))

  val y_np1_i0 = fpu.mul_add_single(mul1=Some(ky1), addend=Some(ky2))
  val y_np1_r = fpu.mul_add_single(mul1=Some(y_np1_i0), mul2=Some(half_r), addend=Some(y_r))

  io.x_np1 := fpu.recfp_to_fp(x_np1_r)
  io.y_np1 := fpu.recfp_to_fp(y_np1_r)
}

class LVRK4_4Stage(fpu: FPU) extends LVRK4(fpu) {
  val maus = new MulAddArray(fpu, num_units=11)

  val r_kx1 = Reg(Bits(float_width))
  val r_ky1 = Reg(Bits(float_width))
  val r_kx2 = Reg(Bits(float_width))
  val r_ky2 = Reg(Bits(float_width))
  val r_kx3 = Reg(Bits(float_width))
  val r_ky3 = Reg(Bits(float_width))
  val r_kx4 = Reg(Bits(float_width))
  val r_interim = Reg(Bits(float_width))

  switch(io.stage) {
    is(0.U) {
      val kx1_i0 = maus.mul_add(i=0, mul1=Some(co_neg_b_r), mul2=Some(y_r), addend=Some(co_lambda_r))
      val kx1_i1 = maus.mul_add(i=1, mul1=Some(kx1_i0), mul2=Some(x_r))
      val kx1 = maus.mul_add(i=2, mul1=Some(kx1_i1), mul2=Some(h_r))

      val ky1_i0 = maus.mul_add(i=3, mul1=Some(co_c_r), mul2=Some(x_r), addend=Some(co_neg_mu_r))
      val ky1_i1 = maus.mul_add(i=4, mul1=Some(ky1_i0), mul2=Some(y_r))
      val ky1 = maus.mul_add(i=5, mul1=Some(ky1_i1), mul2=Some(h_r))

      val kx2_i00 = maus.mul_add(i=6, mul1=Some(kx1), mul2=Some(half_r), addend=Some(x_r))
      val kx2_i01 = maus.mul_add(i=7, mul1=Some(kx2_i00), mul2=Some(h_r))
      val kx2_i10 = maus.mul_add(i=8, mul1=Some(ky1), mul2=Some(half_r), addend=Some(y_r))
      val kx2_i11 = maus.mul_add(i=9, mul1=Some(kx2_i10), mul2=Some(co_neg_b_r), addend=Some(co_lambda_r))
      val kx2 = maus.mul_add(i=10, mul1=Some(kx2_i01), mul2=Some(kx2_i11))

      r_kx1 := kx1
      r_ky1 := ky1
      r_kx2 := kx2
    }
    is(1.U) {
      val kx1 = r_kx1
      val ky1 = r_ky1
      val kx2 = r_kx2

      val ky2_i00 = maus.mul_add(i=0, mul1=Some(ky1), mul2=Some(half_r), addend=Some(y_r))
      val ky2_i01 = maus.mul_add(i=1, mul1=Some(ky2_i00), mul2=Some(h_r))
      val ky2_i10 = maus.mul_add(i=2, mul1=Some(kx1), mul2=Some(half_r), addend=Some(x_r))
      val ky2_i11 = maus.mul_add(i=3, mul1=Some(ky2_i10), mul2=Some(co_c_r), addend=Some(co_neg_mu_r))
      val ky2 = maus.mul_add(i=4, mul1=Some(ky2_i01), mul2=Some(ky2_i11))

      val kx3_i00 = maus.mul_add(i=5, mul1=Some(kx2), mul2=Some(half_r), addend=Some(x_r))
      val kx3_i01 = maus.mul_add(i=6, mul1=Some(kx3_i00), mul2=Some(h_r))
      val kx3_i10 = maus.mul_add(i=7, mul1=Some(ky2), mul2=Some(half_r), addend=Some(y_r))
      val kx3_i11 = maus.mul_add(i=8, mul1=Some(kx3_i10), mul2=Some(co_neg_b_r), addend=Some(co_lambda_r))
      val kx3 = maus.mul_add(i=9, mul1=Some(kx3_i01), mul2=Some(kx3_i11))

      val ky3_i00 = maus.mul_add(i=10, mul1=Some(ky2), mul2=Some(half_r), addend=Some(y_r))

      r_ky2 := ky2
      r_kx3 := kx3
      r_interim := ky3_i00
    }
    is(2.U) {
      val kx2 = r_kx2
      val ky2 = r_ky2
      val kx3 = r_kx3
      val ky3_i00 = r_interim

      val ky3_i01 = maus.mul_add(i=0, mul1=Some(ky3_i00), mul2=Some(h_r))
      val ky3_i10 = maus.mul_add(i=1, mul1=Some(kx2), mul2=Some(half_r), addend=Some(x_r))
      val ky3_i11 = maus.mul_add(i=2, mul1=Some(ky3_i10), mul2=Some(co_c_r), addend=Some(co_neg_mu_r))
      val ky3 = maus.mul_add(i=3, mul1=Some(ky3_i01), mul2=Some(ky3_i11))

      val kx4_i00 = maus.mul_add(i=4, mul1=Some(kx3), addend=Some(x_r))
      val kx4_i01 = maus.mul_add(i=5, mul1=Some(kx4_i00), mul2=Some(h_r))
      val kx4_i10 = maus.mul_add(i=6, mul1=Some(ky3), addend=Some(y_r))
      val kx4_i11 = maus.mul_add(i=7, mul1=Some(kx4_i10), mul2=Some(co_neg_b_r), addend=Some(co_lambda_r))
      val kx4 = maus.mul_add(i=8, mul1=Some(kx4_i01), mul2=Some(kx4_i11))

      val ky4_i00 = maus.mul_add(i=9, mul1=Some(ky3), addend=Some(y_r))
      val ky4_i01 = maus.mul_add(i=10, mul1=Some(ky4_i00), mul2=Some(h_r))

      r_ky3 := ky3
      r_kx4 := kx4
      r_interim := ky4_i01
    }
    is(3.U) {
      val kx1 = r_kx1
      val ky1 = r_ky1
      val kx2 = r_kx2
      val ky2 = r_ky2
      val kx3 = r_kx3
      val ky3 = r_ky3
      val kx4 = r_kx4
      val ky4_i01 = r_interim

      val ky4_i10 = maus.mul_add(i=0, mul1=Some(kx3), addend=Some(x_r))
      val ky4_i11 = maus.mul_add(i=1, mul1=Some(ky4_i10), mul2=Some(co_c_r), addend=Some(co_neg_mu_r))
      val ky4 = maus.mul_add(i=2, mul1=Some(ky4_i01), mul2=Some(ky4_i11))

      val x_np1_i0 = maus.mul_add(i=3, mul1=Some(kx2), mul2=Some(two_r), addend=Some(kx1))
      val x_np1_i1 = maus.mul_add(i=4, mul1=Some(kx3), mul2=Some(two_r), addend=Some(kx4))
      val x_np1_i2 = maus.mul_add(i=5, mul1=Some(x_np1_i0), addend=Some(x_np1_i1))
      val x_np1_r = maus.mul_add(i=6, mul1=Some(x_np1_i2), mul2=Some(sixth_r), addend=Some(x_r))

      val y_np1_i0 = maus.mul_add(i=7, mul1=Some(ky2), mul2=Some(two_r), addend=Some(ky1))
      val y_np1_i1 = maus.mul_add(i=8, mul1=Some(ky3), mul2=Some(two_r), addend=Some(ky4))
      val y_np1_i2 = maus.mul_add(i=9, mul1=Some(y_np1_i0), addend=Some(y_np1_i1))
      val y_np1_r = maus.mul_add(i=10, mul1=Some(y_np1_i2), mul2=Some(sixth_r), addend=Some(y_r))

      io.x_np1 := fpu.recfp_to_fp(x_np1_r)
      io.y_np1 := fpu.recfp_to_fp(y_np1_r)
    }
    is(4.U) { } //inhibit stage for piplining
  }
}

class LVRK4_2Stage(fpu: FPU) extends LVRK4(fpu) {
  val maus = new MulAddArray(fpu, num_units=22)

  val r_kx1 = Reg(Bits(float_width))
  val r_ky1 = Reg(Bits(float_width))
  val r_kx2 = Reg(Bits(float_width))
  val r_ky2 = Reg(Bits(float_width))
  val r_kx3 = Reg(Bits(float_width))
  val r_interim = Reg(Bits(float_width))

  switch(io.stage) {
    is(0.U) {
      val kx1_i0 = maus.mul_add(i=0, mul1=Some(co_neg_b_r), mul2=Some(y_r), addend=Some(co_lambda_r))
      val kx1_i1 = maus.mul_add(i=1, mul1=Some(kx1_i0), mul2=Some(x_r))
      val kx1 = maus.mul_add(i=2, mul1=Some(kx1_i1), mul2=Some(h_r))

      val ky1_i0 = maus.mul_add(i=3, mul1=Some(co_c_r), mul2=Some(x_r), addend=Some(co_neg_mu_r))
      val ky1_i1 = maus.mul_add(i=4, mul1=Some(ky1_i0), mul2=Some(y_r))
      val ky1 = maus.mul_add(i=5, mul1=Some(ky1_i1), mul2=Some(h_r))

      val kx2_i00 = maus.mul_add(i=6, mul1=Some(kx1), mul2=Some(half_r), addend=Some(x_r))
      val kx2_i01 = maus.mul_add(i=7, mul1=Some(kx2_i00), mul2=Some(h_r))
      val kx2_i10 = maus.mul_add(i=8, mul1=Some(ky1), mul2=Some(half_r), addend=Some(y_r))
      val kx2_i11 = maus.mul_add(i=9, mul1=Some(kx2_i10), mul2=Some(co_neg_b_r), addend=Some(co_lambda_r))
      val kx2 = maus.mul_add(i=10, mul1=Some(kx2_i01), mul2=Some(kx2_i11))

      val ky2_i00 = maus.mul_add(i=11, mul1=Some(ky1), mul2=Some(half_r), addend=Some(y_r))
      val ky2_i01 = maus.mul_add(i=12, mul1=Some(ky2_i00), mul2=Some(h_r))
      val ky2_i10 = maus.mul_add(i=13, mul1=Some(kx1), mul2=Some(half_r), addend=Some(x_r))
      val ky2_i11 = maus.mul_add(i=14, mul1=Some(ky2_i10), mul2=Some(co_c_r), addend=Some(co_neg_mu_r))
      val ky2 = maus.mul_add(i=15, mul1=Some(ky2_i01), mul2=Some(ky2_i11))

      val kx3_i00 = maus.mul_add(i=16, mul1=Some(kx2), mul2=Some(half_r), addend=Some(x_r))
      val kx3_i01 = maus.mul_add(i=17, mul1=Some(kx3_i00), mul2=Some(h_r))
      val kx3_i10 = maus.mul_add(i=18, mul1=Some(ky2), mul2=Some(half_r), addend=Some(y_r))
      val kx3_i11 = maus.mul_add(i=19, mul1=Some(kx3_i10), mul2=Some(co_neg_b_r), addend=Some(co_lambda_r))
      val kx3 = maus.mul_add(i=20, mul1=Some(kx3_i01), mul2=Some(kx3_i11))

      val ky3_i00 = maus.mul_add(i=21, mul1=Some(ky2), mul2=Some(half_r), addend=Some(y_r))

      r_kx1 := kx1
      r_ky1 := ky1
      r_kx2 := kx2
      r_ky2 := ky2
      r_kx3 := kx3
      r_interim := ky3_i00
    }
    is(1.U) {
      val kx1 = r_kx1
      val ky1 = r_ky1
      val kx2 = r_kx2
      val ky2 = r_ky2
      val kx3 = r_kx3
      val ky3_i00 = r_interim

      val ky3_i01 = maus.mul_add(i=0, mul1=Some(ky3_i00), mul2=Some(h_r))
      val ky3_i10 = maus.mul_add(i=1, mul1=Some(kx2), mul2=Some(half_r), addend=Some(x_r))
      val ky3_i11 = maus.mul_add(i=2, mul1=Some(ky3_i10), mul2=Some(co_c_r), addend=Some(co_neg_mu_r))
      val ky3 = maus.mul_add(i=3, mul1=Some(ky3_i01), mul2=Some(ky3_i11))

      val kx4_i00 = maus.mul_add(i=4, mul1=Some(kx3), addend=Some(x_r))
      val kx4_i01 = maus.mul_add(i=5, mul1=Some(kx4_i00), mul2=Some(h_r))
      val kx4_i10 = maus.mul_add(i=6, mul1=Some(ky3), addend=Some(y_r))
      val kx4_i11 = maus.mul_add(i=7, mul1=Some(kx4_i10), mul2=Some(co_neg_b_r), addend=Some(co_lambda_r))
      val kx4 = maus.mul_add(i=8, mul1=Some(kx4_i01), mul2=Some(kx4_i11))

      val ky4_i00 = maus.mul_add(i=9, mul1=Some(ky3), addend=Some(y_r))
      val ky4_i01 = maus.mul_add(i=10, mul1=Some(ky4_i00), mul2=Some(h_r))
      val ky4_i10 = maus.mul_add(i=11, mul1=Some(kx3), addend=Some(x_r))
      val ky4_i11 = maus.mul_add(i=12, mul1=Some(ky4_i10), mul2=Some(co_c_r), addend=Some(co_neg_mu_r))
      val ky4 = maus.mul_add(i=13, mul1=Some(ky4_i01), mul2=Some(ky4_i11))

      val x_np1_i0 = maus.mul_add(i=14, mul1=Some(kx2), mul2=Some(two_r), addend=Some(kx1))
      val x_np1_i1 = maus.mul_add(i=15, mul1=Some(kx3), mul2=Some(two_r), addend=Some(kx4))
      val x_np1_i2 = maus.mul_add(i=16, mul1=Some(x_np1_i0), addend=Some(x_np1_i1))
      val x_np1_r = maus.mul_add(i=17, mul1=Some(x_np1_i2), mul2=Some(sixth_r), addend=Some(x_r))

      val y_np1_i0 = maus.mul_add(i=18, mul1=Some(ky2), mul2=Some(two_r), addend=Some(ky1))
      val y_np1_i1 = maus.mul_add(i=19, mul1=Some(ky3), mul2=Some(two_r), addend=Some(ky4))
      val y_np1_i2 = maus.mul_add(i=20, mul1=Some(y_np1_i0), addend=Some(y_np1_i1))
      val y_np1_r = maus.mul_add(i=21, mul1=Some(y_np1_i2), mul2=Some(sixth_r), addend=Some(y_r))

      io.x_np1 := fpu.recfp_to_fp(x_np1_r)
      io.y_np1 := fpu.recfp_to_fp(y_np1_r)
    }
  }
}

class LVRK4_1Stage(fpu: FPU) extends LVRK4(fpu) {
  val kx1_i0 = fpu.mul_add_single(mul1=Some(co_neg_b_r), mul2=Some(y_r), addend=Some(co_lambda_r))
  val kx1_i1 = fpu.mul_add_single(mul1=Some(kx1_i0), mul2=Some(x_r))
  val kx1 = fpu.mul_add_single(mul1=Some(kx1_i1), mul2=Some(h_r))

  val ky1_i0 = fpu.mul_add_single(mul1=Some(co_c_r), mul2=Some(x_r), addend=Some(co_neg_mu_r))
  val ky1_i1 = fpu.mul_add_single(mul1=Some(ky1_i0), mul2=Some(y_r))
  val ky1 = fpu.mul_add_single(mul1=Some(ky1_i1), mul2=Some(h_r))

  val kx2_i00 = fpu.mul_add_single(mul1=Some(kx1), mul2=Some(half_r), addend=Some(x_r))
  val kx2_i01 = fpu.mul_add_single(mul1=Some(kx2_i00), mul2=Some(h_r))
  val kx2_i10 = fpu.mul_add_single(mul1=Some(ky1), mul2=Some(half_r), addend=Some(y_r))
  val kx2_i11 = fpu.mul_add_single(mul1=Some(kx2_i10), mul2=Some(co_neg_b_r), addend=Some(co_lambda_r))
  val kx2 = fpu.mul_add_single(mul1=Some(kx2_i01), mul2=Some(kx2_i11))

  val ky2_i00 = fpu.mul_add_single(mul1=Some(ky1), mul2=Some(half_r), addend=Some(y_r))
  val ky2_i01 = fpu.mul_add_single(mul1=Some(ky2_i00), mul2=Some(h_r))
  val ky2_i10 = fpu.mul_add_single(mul1=Some(kx1), mul2=Some(half_r), addend=Some(x_r))
  val ky2_i11 = fpu.mul_add_single(mul1=Some(ky2_i10), mul2=Some(co_c_r), addend=Some(co_neg_mu_r))
  val ky2 = fpu.mul_add_single(mul1=Some(ky2_i01), mul2=Some(ky2_i11))

  val kx3_i00 = fpu.mul_add_single(mul1=Some(kx2), mul2=Some(half_r), addend=Some(x_r))
  val kx3_i01 = fpu.mul_add_single(mul1=Some(kx3_i00), mul2=Some(h_r))
  val kx3_i10 = fpu.mul_add_single(mul1=Some(ky2), mul2=Some(half_r), addend=Some(y_r))
  val kx3_i11 = fpu.mul_add_single(mul1=Some(kx3_i10), mul2=Some(co_neg_b_r), addend=Some(co_lambda_r))
  val kx3 = fpu.mul_add_single(mul1=Some(kx3_i01), mul2=Some(kx3_i11))

  val ky3_i00 = fpu.mul_add_single(mul1=Some(ky2), mul2=Some(half_r), addend=Some(y_r))
  val ky3_i01 = fpu.mul_add_single(mul1=Some(ky3_i00), mul2=Some(h_r))
  val ky3_i10 = fpu.mul_add_single(mul1=Some(kx2), mul2=Some(half_r), addend=Some(x_r))
  val ky3_i11 = fpu.mul_add_single(mul1=Some(ky3_i10), mul2=Some(co_c_r), addend=Some(co_neg_mu_r))
  val ky3 = fpu.mul_add_single(mul1=Some(ky3_i01), mul2=Some(ky3_i11))

  val kx4_i00 = fpu.mul_add_single(mul1=Some(kx3), addend=Some(x_r))
  val kx4_i01 = fpu.mul_add_single(mul1=Some(kx4_i00), mul2=Some(h_r))
  val kx4_i10 = fpu.mul_add_single(mul1=Some(ky3), addend=Some(y_r))
  val kx4_i11 = fpu.mul_add_single(mul1=Some(kx4_i10), mul2=Some(co_neg_b_r), addend=Some(co_lambda_r))
  val kx4 = fpu.mul_add_single(mul1=Some(kx4_i01), mul2=Some(kx4_i11))

  val ky4_i00 = fpu.mul_add_single(mul1=Some(ky3), addend=Some(y_r))
  val ky4_i01 = fpu.mul_add_single(mul1=Some(ky4_i00), mul2=Some(h_r))
  val ky4_i10 = fpu.mul_add_single(mul1=Some(kx3), addend=Some(x_r))
  val ky4_i11 = fpu.mul_add_single(mul1=Some(ky4_i10), mul2=Some(co_c_r), addend=Some(co_neg_mu_r))
  val ky4 = fpu.mul_add_single(mul1=Some(ky4_i01), mul2=Some(ky4_i11))

  val x_np1_i0 = fpu.mul_add_single(mul1=Some(kx2), mul2=Some(two_r), addend=Some(kx1))
  val x_np1_i1 = fpu.mul_add_single(mul1=Some(kx3), mul2=Some(two_r), addend=Some(kx4))
  val x_np1_i2 = fpu.mul_add_single(mul1=Some(x_np1_i0), addend=Some(x_np1_i1))
  val x_np1_r = fpu.mul_add_single(mul1=Some(x_np1_i2), mul2=Some(sixth_r), addend=Some(x_r))

  val y_np1_i0 = fpu.mul_add_single(mul1=Some(ky2), mul2=Some(two_r), addend=Some(ky1))
  val y_np1_i1 = fpu.mul_add_single(mul1=Some(ky3), mul2=Some(two_r), addend=Some(ky4))
  val y_np1_i2 = fpu.mul_add_single(mul1=Some(y_np1_i0), addend=Some(y_np1_i1))
  val y_np1_r = fpu.mul_add_single(mul1=Some(y_np1_i2), mul2=Some(sixth_r), addend=Some(y_r))

  io.x_np1 := fpu.recfp_to_fp(x_np1_r)
  io.y_np1 := fpu.recfp_to_fp(y_np1_r)
}

class LVEuler(fpu: FPU) extends Module {
  val io = IO(new Bundle {
    val h = Bits(INPUT)
    val this_var = Bits(INPUT)
    val other_var = Bits(INPUT)
    val co_const = Bits(INPUT)
    val co_other_var = Bits(INPUT)
    val this_var_next = Bits(OUTPUT)
  })

  val h_r = fpu.fp_to_recfp(io.h)
  val this_var_rec = fpu.fp_to_recfp(io.this_var)
  val other_var_rec = fpu.fp_to_recfp(io.other_var)
  val co_const_rec = fpu.fp_to_recfp(io.co_const)
  val co_other_var_rec = fpu.fp_to_recfp(io.co_other_var)

  val interim1 = fpu.mul_add_single(mul1=Some(co_other_var_rec), mul2=Some(other_var_rec), addend=Some(co_const_rec))
  val interim2 = fpu.mul_add_single(mul1=Some(interim1), mul2=Some(this_var_rec))
  val this_var_next_rec = fpu.mul_add_single(mul1=Some(interim2), mul2=Some(h_r), addend=Some(this_var_rec))
  io.this_var_next := fpu.recfp_to_fp(this_var_next_rec)
}

