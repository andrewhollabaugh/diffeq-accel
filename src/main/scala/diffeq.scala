package diffeq

import Chisel._
import freechips.rocketchip.config._
import freechips.rocketchip.diplomacy._
import freechips.rocketchip.rocket._
import freechips.rocketchip.tile._
import freechips.rocketchip.tilelink._

class DiffEqAccelImp(outer: DiffEqAccel, use_dpfp: Boolean = false) extends LazyRoCCModuleImp(outer) {
  val fpu = FPU(use_dpfp)
  val float_width = fpu.w_total

  val r_h = Reg(Bits(float_width))
  val r_num_steps = Reg(UInt(64.W))
  val r_input_addr = Reg(UInt(64.W))
  val r_out_addr = Reg(UInt(64.W))

  val r_x = Reg(Bits(float_width))
  val r_y = Reg(Bits(float_width))
  val r_co_lambda = Reg(Bits(float_width))
  val r_co_neg_b = Reg(Bits(float_width))
  val r_co_neg_mu = Reg(Bits(float_width))
  val r_co_c = Reg(Bits(float_width))

  val r_mem_busy = Reg(init=false.B)
  val r_mem_index = Reg(init=UInt(0, 64.W))

  io.interrupt := false.B

  io.mem.req.bits.size := log2Ceil(8).U
  io.mem.req.bits.phys := false.B
  io.mem.req.bits.signed := false.B
  io.mem.req.bits.dprv := io.cmd.bits.status.dprv

  when(io.mem.req.valid && io.mem.req.ready) {
    r_mem_busy := true.B
  }.elsewhen(io.mem.resp.valid) {
    r_mem_busy := false.B
  }

  //Common states
  val s_idle = 0.U
  val s_read_req = 1.U
  val s_read_resp = 2.U
  val s_read_wait = 3.U

  val r_state = Reg(init=s_idle)

  io.cmd.ready := r_state === s_idle
  io.busy := r_state =/= s_idle
  val mem_req_valid_common = r_state === s_read_req || r_state === s_read_resp

  /* INPUTS ARGS
  * funct=0
  * rs1=step_size
  * rs2=num_steps
  *
  * funct=1 -> run
  * rs1=addr to inputs
  * [x_0, y_0, lambda, -b, -mu, c]
  *  0-0  0-1    1-0   1-1  2-0 2-1
  * rs2=addr to output
  */
  def sf_idle = {
    when(io.cmd.valid) {
      when(io.cmd.bits.inst.funct === 0.U) {
        r_h := io.cmd.bits.rs1
        r_num_steps := io.cmd.bits.rs2
      }.elsewhen(io.cmd.bits.inst.funct === 1.U) {
        r_input_addr := io.cmd.bits.rs1
        r_out_addr := io.cmd.bits.rs2
        r_state := s_read_req
      }
    }
  }

  def sf_read_req = {
    io.mem.req.bits.cmd := M_XRD
    io.mem.req.bits.addr := r_input_addr
    io.mem.req.bits.data := 0.U
    when(io.mem.req.ready) {
      r_input_addr := r_input_addr + 8.U
      r_mem_index := r_mem_index + 1.U
      r_state := s_read_resp
    }
  }

  def sf_read_resp(dpfp: Boolean) = {
    val resp_data = io.mem.resp.bits.data
    val max_index = if(dpfp) 6.U else 3.U

    when(io.mem.resp.valid) {
      when(r_mem_index <= max_index) {
        if(dpfp) {
          switch(r_mem_index) {
            is(1.U) { r_x := resp_data }
            is(2.U) { r_y := resp_data }
            is(3.U) { r_co_lambda := resp_data }
            is(4.U) { r_co_neg_b := resp_data }
            is(5.U) { r_co_neg_mu := resp_data }
            is(6.U) { r_co_c := resp_data }
          }
        } else {
          switch(r_mem_index) {
            is(1.U) {
              r_x := resp_data(31,0)
              r_y := resp_data(63,32)
            }
            is(2.U) {
              r_co_lambda := resp_data(31,0)
              r_co_neg_b := resp_data(63,32)
            }
            is(3.U) {
              r_co_neg_mu := resp_data(31,0)
              r_co_c := resp_data(63,32)
            }
          }
        }
        r_state := s_read_req
      }.otherwise {
        r_mem_index := 0.U
        r_state := s_read_wait
      }
    }
  }

  def sf_write(data: Bits, s_next: UInt, end_num: Option[UInt] = None) = {
    io.mem.req.bits.cmd := M_XWR
    io.mem.req.bits.addr := r_out_addr
    io.mem.req.bits.data := data
    when(io.mem.resp.valid) {
      r_out_addr := r_out_addr + 8.U
      r_mem_index := r_mem_index + 1.U
      if(end_num.isDefined) {
        when(r_mem_index < end_num.get - 1.U) {
          r_state := s_next
        }.otherwise{
          r_mem_index := 0.U
          r_state := s_idle
        }
      } else {
        r_state := s_next
      }
    }
  }

  def common_states(s_next: UInt, dpfp: Boolean = false) = {
    switch(r_state) {
      is(s_idle) { sf_idle }
      is(s_read_req) { sf_read_req }
      is(s_read_resp) { sf_read_resp(dpfp) }
      is(s_read_wait) { r_state := s_next }
    }
  }

  def euler_x(euler_unit: LVEuler, x_in: Bits, y_in: Bits): Bits = {
    euler_unit.io.h := r_h
    euler_unit.io.this_var := x_in
    euler_unit.io.other_var := y_in
    euler_unit.io.co_const := r_co_lambda
    euler_unit.io.co_other_var := r_co_neg_b
    euler_unit.io.this_var_next
  }

  def euler_y(euler_unit: LVEuler, x_in: Bits, y_in: Bits): Bits = {
    euler_unit.io.h := r_h
    euler_unit.io.this_var := y_in
    euler_unit.io.other_var := x_in
    euler_unit.io.co_const := r_co_neg_mu
    euler_unit.io.co_other_var := r_co_c
    euler_unit.io.this_var_next
  }

  def euler_x_y(euler_unit_x: LVEuler, euler_unit_y: LVEuler, x_in: Bits, y_in: Bits): (Bits, Bits) = {
    (euler_x(euler_unit_x, x_in, y_in), euler_y(euler_unit_y, x_in, y_in))
  }

  def rk_set_inputs(rk_unit: LVRK) = {
    rk_unit.io.h := r_h
    rk_unit.io.x := r_x
    rk_unit.io.y := r_y
    rk_unit.io.co_lambda := r_co_lambda
    rk_unit.io.co_neg_b := r_co_neg_b
    rk_unit.io.co_neg_mu := r_co_neg_mu
    rk_unit.io.co_c := r_co_c
  }
}

