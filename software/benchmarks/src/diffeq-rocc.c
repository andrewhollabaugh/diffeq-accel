#include <stdio.h>
#include <stdint.h>
#include "rocc.h"
#include "diffeq.h"
#include "encoding.h"
#include "compiler.h"

int main() {
  unsigned long start, end;

  do {
    printf("Start basic test 1.\n");

    start = rdcycle();

    asm volatile ("fence");

    ROCC_INSTRUCTION_SS(2, step_size, num_steps, 0);
    ROCC_INSTRUCTION_SS(2, &inputs, &output, 1);

    asm volatile ("fence" ::: "memory");

    end = rdcycle();

#ifdef PRINT_OUTPUT
    test_output();
#endif

  } while(0);

  printf("Success!\n");

  printf("Execution took %lu cycles\n", end - start);

  return 0;
}
