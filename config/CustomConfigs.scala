package chipyard

import freechips.rocketchip.config.{Config}
import freechips.rocketchip.diplomacy.{AsynchronousCrossing}

class DiffEqConfig extends Config(
  new diffeq.WithDiffEqAccel ++
  new freechips.rocketchip.subsystem.WithNBigCores(1) ++
  new chipyard.config.AbstractConfig)

class DiffEqConfigDouble extends Config(
  new diffeq.WithDiffEqAccel(use_double=true) ++
  new freechips.rocketchip.subsystem.WithNBigCores(1) ++
  new chipyard.config.AbstractConfig)

class DiffEqConfigDualCompute extends Config(
  new diffeq.WithDiffEqAccel(use_dual_compute=true) ++
  new freechips.rocketchip.subsystem.WithNBigCores(1) ++
  new chipyard.config.AbstractConfig)

class DiffEqConfigShortInt extends Config(
  new diffeq.WithDiffEqAccel(use_short_int=true) ++
  new freechips.rocketchip.subsystem.WithNBigCores(1) ++
  new chipyard.config.AbstractConfig)

class DiffEqConfigShortIntDualCompute extends Config(
  new diffeq.WithDiffEqAccel(use_short_int=true, use_dual_compute=true) ++
  new freechips.rocketchip.subsystem.WithNBigCores(1) ++
  new chipyard.config.AbstractConfig)

class DiffEqConfigShortIntQuadCompute extends Config(
  new diffeq.WithDiffEqAccel(use_short_int=true, use_quad_compute=true) ++
  new freechips.rocketchip.subsystem.WithNBigCores(1) ++
  new chipyard.config.AbstractConfig)

class DiffEqConfigRK4 extends Config(
  new diffeq.WithDiffEqAccel(use_rk4=true) ++
  new freechips.rocketchip.subsystem.WithNBigCores(1) ++
  new chipyard.config.AbstractConfig)

