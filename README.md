# diffeq-accel

A hardware accelerator written in Chisel to solve the Lotka-Volterra family of equations using numerical methods. Developed using Chisel, a high-level HDL based on Scala using the Chipyard environment.


